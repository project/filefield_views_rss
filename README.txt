DESCRIPTION
===========

This is a CCK module that automatically wraps linked media files with enclosure tags added using the CCK Filefield module.

This module is designed to work with Drupal 5.x and requires PHP 5.

DEPENDENCIES
============

Requires the Content, Filefield, Views, and Views RSS modules.

INSTALLATION
============

1. Place the module in the modules directory.
2. Activate the module. 
3. Click on Fieldfield Views RSS under site configuration.
4. Select the content types you require.
5. Enter in the fields from your content type that you require.

NOTE: Do not add the "field_" prefix on each of the fields.

AUTHOR
======
roberto.gerola@speedtech.it

SPONSOR
=======
Left Right Minds
http://www.leftrightminds.com